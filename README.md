# University Planner #

## Team members ##

Naveen Kumar Chandaluri
Manohar Sriram
Rahul Reddy Lankala
Keerthi Chiduruppa

## What is this repository for? ##

The “University Planner” is a native android application that brings change to the conventional method of
searching Universities and their requirements manually. This application reduces lot of time and workload 
by providing the details of the Universities by just selecting the state which gives the information of all
the Universities in that state, its offered courses and their requirements. It provides multiple user
accessibility and also has different user privileges. This system uses centralized databases, which provides
easy retrieval of data.

### Administrator ###
Admin is the main authority of this site, so he has all the privileges. This homepage has two options of whether it is an existing or a new university.

The administrator can perform following functions:

---	He can add details of the Universities and their courses.

--- He can delete the details of the Universities and their courses.

--- He can update the details of the Universities and their courses.

### User ###

--- User does not need to register to get started with the application.

--- After user launch the application, he is provided with selection tab where he can search for the Universities and their requirements based on the state he entered. The selection has a dropdown menu which contains the list of states. 

•	As the user selects the state he is provided with a list of all the universities in that state.

•	As the user selects a particular University, he is provided with a description of all the courses it offers.

•	It also provides information about the requirements of the University like minimum GRE score, IELTS/TOEFL score, etc.

## How do I get set up? ##

## Installation ##
Clone the repository and import into Android Studio.

## Test credentials- ##
Admin username: admin@gmail.com
Password: admin123

## Supported devices: ##
google nexus 5 – 4.4.4 – API 19 – 1080X1920

## Database: ##
There is a common database for both the applications i.e Google FireBase. 
For admin: The details of all the universities are stored in the database using the administrator application.
For user: The user can retrieve the details stored in the database by the administrator.

## Sequence information ##
1.	Open Android Studio and launch the Android SDK manager from it
2.	Select Open an existing Android Studio project
3.	Choose the project directory.
4.	Run the application.

## Contribution of each individual ##

### Naveen Kumar Chanduluri ###
 
Created University Planner for both Admin and User Android applications.

Added labels and spinners as part of the create universtiy layout.

Modified User layout.

Added save Button to the create layout.

Created delete Activity and added Alert Dialog.

Created Modified Activity.

Changed layouts for the university Admin.

Added Login Page.

Created SignUp Page.

Added functionality to the login page.

Added state details to the spinner in the create university details.

Functionality select state then popup the university details of particular state.

Validation for state, univiersity and course.

Added functionality for save details for university planner.

Added dummy data and functionality to the delete details.

Started using google firebase.

Added google fire base for login and verified.

Added Google firebase database to create universities.

Button functionality for delete operation.

Connceted to google firebase for delete.

Added google firebase for scores.

Changed changes for delete operation.

Added google firebase for the modified.

Added modified functionality to update modified details.


### Manohar Sriram ###

Modified Labels and Layouts to the University Planner Application.

Modified Title of the Application.

Created spinners to the University Plannery User Layout.

created tab layout with three tabs named selection update delete.

created grid layout with few views like GRE score, IELTS,TOEFL , University link and also buttons to find and reset.

Created logo and added to the start page.

Removed unknown folder.

Added dummy data and functionality to the user application.

Modified logic for update page in Admin.

Modified logic for update page in Admin.

created a logout button to the welcome page of admin.

Added sigout button and functionality.

Added Logout button.

Logout functionality for create delete and modify.


### Rahul Reddy Lankala ###

Created layout to create or add university details in Admin.

Created seperate layouts for create,modify and delete each for university Planner for admin.	

Changed tab layout at user application.

Added alert Dialog box to the update button at the modify page.

Added alert Dialog box to the update button at the delete page.

Added functionlity and dummy data.

added dummy details for the college.

Modified changes at modified page.

Modified xml files and added background image.
	
Logic has been changed in the modified page for course selection.

Logic has been changed in the modified page for course selection.

Modified functionality to the course in the delete page.

New functionality to the course spinner for user application


### Keerthi Chiduruppa ###

Added three buttons for Create,Modify and Delete for University Planner Admin Application Approvals. 

Added alert Dialog to the create button at the create page.

modified home, signUp and signIn page.

modified signup and admin pages.

README.md created.

README.md edited online with Bitbucket.

Added model class for user application.

Connected to the google fire base and get the results.

Modified layout of the user.

Updated user module interface.

README.md edited online with Bitbucket.

